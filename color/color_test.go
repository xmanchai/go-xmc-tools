package color

import (
	"image/color"
	"reflect"
	"testing"
)

func TestHex2RGBA(t *testing.T) {
	type args struct {
		hex string
	}
	var tests []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Hex2RGBA(tt.args.hex)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hex2RGBA() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hex2RGBA() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHex_Hex2RGB(t *testing.T) {
	type fields struct {
		String string
		Alpha  float64
	}
	var tests []struct {
		name    string
		fields  fields
		want    interface{}
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := Hex{
				String: tt.fields.String,
				Alpha:  tt.fields.Alpha,
			}
			got, err := h.Hex2RGB()
			if (err != nil) != tt.wantErr {
				t.Errorf("Hex2RGB() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hex2RGB() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHex_Hex2RGBA(t *testing.T) {
	type fields struct {
		String string
		Alpha  float64
	}
	var tests []struct {
		name    string
		fields  fields
		want    interface{}
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := Hex{
				String: tt.fields.String,
				Alpha:  tt.fields.Alpha,
			}
			got, err := h.Hex2RGBA()
			if (err != nil) != tt.wantErr {
				t.Errorf("Hex2RGBA() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hex2RGBA() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRGB2Hex(t *testing.T) {
	type args struct {
		c color.Color
	}
	var tests []struct {
		name string
		args args
		want Hex
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RGB2Hex(tt.args.c); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RGB2Hex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test__int2hex(t *testing.T) {
	type args struct {
		i int64
	}
	var tests []struct {
		name string
		args args
		want string
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := _int2hex(tt.args.i); got != tt.want {
				t.Errorf("_int2hex() = %v, want %v", got, tt.want)
			}
		})
	}
}
