package color

import (
	"errors"
	"image/color"
	"regexp"
	"strconv"
)

type Hex struct {
	String string
	Alpha  float64
}

// Hex2RGB 函数名称
//  @描述: 默认透明值为255 == 1
//  @结构 h
//  @返回值 interface{}
//  @返回值 error
//
func (h Hex) Hex2RGB() (interface{}, error) {
	match, _ := regexp.MatchString("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", h.String)
	if !match {
		return nil, errors.New("not match hex color string")
	}
	r, _ := strconv.ParseInt(h.String[1:3], 16, 10)
	g, _ := strconv.ParseInt(h.String[3:5], 16, 10)
	b, _ := strconv.ParseInt(h.String[5:], 16, 10)
	return color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b), A: 255}, nil
}

func (h Hex) Hex2RGBA() (interface{}, error) {
	match, _ := regexp.MatchString("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", h.String)
	alpha := h.Alpha * 255
	if !match {
		return nil, errors.New("not match hex color string")
	}
	r, _ := strconv.ParseInt(h.String[1:3], 16, 10)
	g, _ := strconv.ParseInt(h.String[3:5], 16, 10)
	b, _ := strconv.ParseInt(h.String[5:], 16, 10)
	return color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b), A: uint8(alpha)}, nil
}

// Hex2RGBA 函数名称
//  @描述: 透明度需要经过计算float64 * 255 转uint8
//  @返回值 interface{}
//  @返回值 error
//
func Hex2RGBA(hex string) (interface{}, error) {
	match, _ := regexp.MatchString("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", hex)
	if !match {
		return nil, errors.New("not match hex color string")
	}
	r, _ := strconv.ParseInt(hex[1:3], 16, 10)
	g, _ := strconv.ParseInt(hex[3:5], 16, 10)
	b, _ := strconv.ParseInt(hex[5:], 16, 10)
	return color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b), A: uint8(255)}, nil
}

// RGB2Hex 函数名称
//  @描述:
//  @参数 c
//  @返回值 Hex
//
func RGB2Hex(c color.Color) Hex {
	r, g, b, a := c.RGBA()
	red := _int2hex(int64(r >> 8))
	green := _int2hex(int64(g >> 8))
	blue := _int2hex(int64(b >> 8))
	alpha := a / 255
	return Hex{String: red + green + blue, Alpha: float64(alpha)}
}

func _int2hex(i int64) string {
	r := strconv.FormatInt(i, 16)
	if len(r) == 1 {
		r = "0" + r
	}
	return r
}
