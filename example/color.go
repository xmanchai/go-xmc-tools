package main

import (
	"fmt"
	gxtColor "gitee.com/xmanchai/go-xmc-tools/color"
	"image/color"
)

func main() {
	hex := gxtColor.Hex{String: "#eb2a3c", Alpha: 0.23}
	pst, err := hex.Hex2RGB()
	rst, err := gxtColor.Hex2RGBA("#ff2006")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Type is %T\nValue is %v\n", pst, pst)
	fmt.Printf("Type is %T\nValue is %v\n", rst, rst)
	c := color.RGBA{R: 255, G: 32, B: 6, A: 25}
	fmt.Println(gxtColor.RGB2Hex(c))
}
